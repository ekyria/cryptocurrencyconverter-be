﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CryptoCurrencyConverter.Service.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    symbol = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "priceSource",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    url = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    parameterFormat = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    createdDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    updatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_priceSource", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "requestedProperty",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    propertyName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_requestedProperty", x => x.id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "availableCurrencyPair",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    priceSourceId = table.Column<int>(type: "int", nullable: false),
                    fromCurrencyId = table.Column<int>(type: "int", nullable: false),
                    toCurrencyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_availableCurrencyPair", x => x.id);
                    table.ForeignKey(
                        name: "FK_availableCurrencyPair_Currency_fromCurrencyId",
                        column: x => x.fromCurrencyId,
                        principalTable: "Currency",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_availableCurrencyPair_Currency_toCurrencyId",
                        column: x => x.toCurrencyId,
                        principalTable: "Currency",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_availableCurrencyPair_priceSource_priceSourceId",
                        column: x => x.priceSourceId,
                        principalTable: "priceSource",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "priceSourceResponse",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    priceSourceId = table.Column<int>(type: "int", nullable: false),
                    requestedPropertyId = table.Column<int>(type: "int", nullable: false),
                    responsePropertyName = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_priceSourceResponse", x => x.id);
                    table.ForeignKey(
                        name: "FK_priceSourceResponse_priceSource_priceSourceId",
                        column: x => x.priceSourceId,
                        principalTable: "priceSource",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_priceSourceResponse_requestedProperty_requestedPropertyId",
                        column: x => x.requestedPropertyId,
                        principalTable: "requestedProperty",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_availableCurrencyPair_fromCurrencyId",
                table: "availableCurrencyPair",
                column: "fromCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_availableCurrencyPair_priceSourceId",
                table: "availableCurrencyPair",
                column: "priceSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_availableCurrencyPair_toCurrencyId",
                table: "availableCurrencyPair",
                column: "toCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Currency_name",
                table: "Currency",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Currency_symbol",
                table: "Currency",
                column: "symbol",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_priceSource_name",
                table: "priceSource",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_priceSource_url",
                table: "priceSource",
                column: "url",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_priceSourceResponse_priceSourceId",
                table: "priceSourceResponse",
                column: "priceSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_priceSourceResponse_requestedPropertyId",
                table: "priceSourceResponse",
                column: "requestedPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_requestedProperty_propertyName",
                table: "requestedProperty",
                column: "propertyName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "availableCurrencyPair");

            migrationBuilder.DropTable(
                name: "priceSourceResponse");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropTable(
                name: "priceSource");

            migrationBuilder.DropTable(
                name: "requestedProperty");
        }
    }
}
