﻿using CryptoCurrencyConverter.Service.Data.EntityConfigurations;
using CryptoCurrencyConverter.Service.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Data.Db
{
    public partial class CryptoConverterDbContext : DbContext
    {
        public CryptoConverterDbContext(DbContextOptions<CryptoConverterDbContext> options) : base(options)
        {
        }

        #region SaveChanges Override

        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((BaseEntity)entityEntry.Entity).updatedDate = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).createdDate = DateTime.Now;
                }
            }
            return base.SaveChanges();
        }
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((BaseEntity)entityEntry.Entity).updatedDate = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).createdDate = DateTime.Now;
                }
            }
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        #endregion
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PriceSourceEntityConfiguration());
            builder.ApplyConfiguration(new AvailableCurrencyPairConfiguration());
            builder.ApplyConfiguration(new CurrencyEntityConfiguration());
            builder.ApplyConfiguration(new PriceSourceResponseEntityConfiguration());
            builder.ApplyConfiguration(new RequestedPropertyEntityConfiguration());
        }

        public virtual DbSet<PriceSource> PriceSources { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<AvailableCurrencyPair> AvailableCurrencyPairs { get; set; }
        public virtual DbSet<RequestedProperty> RequestedProperties { get; set; }
        public virtual DbSet<PriceSourceResponse> PriceSourceResponses{ get; set; }
    }
}
