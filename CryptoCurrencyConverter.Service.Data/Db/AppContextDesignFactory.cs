﻿using Domain.Core.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace CryptoCurrencyConverter.Service.Data.Db
{
    public partial class CryptoConverterDbContext
    {
        public class AppContextDesignFactory : IDesignTimeDbContextFactory<CryptoConverterDbContext>
        {
            public CryptoConverterDbContext CreateDbContext(string[] args)
            {
                Console.WriteLine(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "CryptoCurrencyConverter.Service.Api"));
                var configuration = new ConfigurationBuilder()
                               .SetBasePath(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "CryptoCurrencyConverter.Service.Api"))
                               .AddJsonFile("appsettings.json", optional: false)
                               .Build();

                var settingsSection = configuration.GetSection("DbConnectionSettings");
                var dbConnectionSettings = new DbConnectionSettings();
                settingsSection.Bind(dbConnectionSettings);

                Console.WriteLine(dbConnectionSettings.BuildConnectionString());

                return new CryptoConverterDbContext(new DbContextOptionsBuilder<CryptoConverterDbContext>()
                    .UseMySql(dbConnectionSettings.BuildConnectionString(),
                    ServerVersion.AutoDetect(dbConnectionSettings.BuildConnectionString()))
                    .Options);
            }
        }
    }
}
