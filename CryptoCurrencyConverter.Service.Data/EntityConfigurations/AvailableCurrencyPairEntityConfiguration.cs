﻿using CryptoCurrencyConverter.Service.Domain.Models;
using Domain.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CryptoCurrencyConverter.Service.Data.EntityConfigurations
{
    class AvailableCurrencyPairConfiguration : IEntityTypeConfiguration<AvailableCurrencyPair>
    {
        public void Configure(EntityTypeBuilder<AvailableCurrencyPair> builder)
        {
            builder.ToTable("availableCurrencyPair");

            builder.HasKey(k => k.id);

            builder.AllProperties(x => x.PropertyType == typeof(string))
                .ForEach(x =>
                {
                    x.IsRequired();
                    x.HasMaxLength(100);
                });
        }
    }
}
