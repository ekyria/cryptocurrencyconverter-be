﻿using CryptoCurrencyConverter.Service.Domain.Models;
using Domain.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CryptoCurrencyConverter.Service.Data.EntityConfigurations
{
    class PriceSourceEntityConfiguration : IEntityTypeConfiguration<PriceSource>
    {
        public void Configure(EntityTypeBuilder<PriceSource> builder)
        {
            builder.ToTable("priceSource");

            builder.HasKey(k => k.id);
            builder.HasIndex(i => i.name).IsUnique();
            builder.HasIndex(i => i.url).IsUnique();

            builder.HasMany(ps => ps.availableCurrencyPairs)
                .WithOne(cp => cp.priceSource)
                .HasForeignKey(cp => cp.priceSourceId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(ps => ps.priceSourceReponses)
                .WithOne(cp => cp.priceSource)
                .HasForeignKey(cp => cp.priceSourceId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.AllProperties(x => x.PropertyType == typeof(string))
                .ForEach(x =>
                {
                    x.IsRequired();
                    x.HasMaxLength(100);
                });
        }
    }
}
