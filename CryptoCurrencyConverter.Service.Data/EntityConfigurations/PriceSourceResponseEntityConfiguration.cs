﻿using CryptoCurrencyConverter.Service.Domain.Models;
using Domain.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CryptoCurrencyConverter.Service.Data.EntityConfigurations
{
    class PriceSourceResponseEntityConfiguration : IEntityTypeConfiguration<PriceSourceResponse>
    {
        public void Configure(EntityTypeBuilder<PriceSourceResponse> builder)
        {
            builder.ToTable("priceSourceResponse");

            builder.HasKey(k => k.id);

            builder.AllProperties(x => x.PropertyType == typeof(string))
                .ForEach(x =>
                {
                    x.IsRequired();
                    x.HasMaxLength(100);
                });
        }
    }
}
