﻿using CryptoCurrencyConverter.Service.Domain.Models;
using Domain.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CryptoCurrencyConverter.Service.Data.EntityConfigurations
{
    class CurrencyEntityConfiguration : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            builder.ToTable("Currency");

            builder.HasKey(k => k.id);
            builder.HasIndex(i => i.name).IsUnique();
            builder.HasIndex(i => i.symbol).IsUnique();

            builder.HasMany(cp => cp.fromAvailableCurrencyPairs)
                .WithOne(c => c.fromCurrency)
                .HasForeignKey(cp => cp.fromCurrencyId);

            builder.HasMany(cp => cp.toAvailableCurrencyPairs)
                .WithOne(c => c.toCurrency)
                .HasForeignKey(cp => cp.toCurrencyId);

            builder.AllProperties(x => x.PropertyType == typeof(string))
                .ForEach(x =>
                {
                    x.IsRequired();
                    x.HasMaxLength(100);
                });
        }
    }
}
