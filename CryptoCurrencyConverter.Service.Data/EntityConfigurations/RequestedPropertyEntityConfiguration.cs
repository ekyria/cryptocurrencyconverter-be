﻿using CryptoCurrencyConverter.Service.Domain.Models;
using Domain.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CryptoCurrencyConverter.Service.Data.EntityConfigurations
{
    class RequestedPropertyEntityConfiguration : IEntityTypeConfiguration<RequestedProperty>
    {
        public void Configure(EntityTypeBuilder<RequestedProperty> builder)
        {
            builder.ToTable("requestedProperty");

            builder.HasKey(k => k.id);
            builder.HasIndex(i => i.propertyName).IsUnique();

            builder.HasMany(rp => rp.priceSourceResponses)
                .WithOne(psr => psr.requestedProperty)
                .HasForeignKey(psr => psr.requestedPropertyId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.AllProperties(x => x.PropertyType == typeof(string))
                .ForEach(x =>
                {
                    x.IsRequired();
                    x.HasMaxLength(100);
                });
        }
    }
}
