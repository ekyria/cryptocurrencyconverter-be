﻿using CryptoCurrencyConverter.Service.Data.Db;
using CryptoCurrencyConverter.Service.Domain.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Data.Repositories
{
    public class PriceSourceRepository : Repository<PriceSource>, IPriceSourceRepository
    {
        private readonly CryptoConverterDbContext _dbContext;
        public PriceSourceRepository(CryptoConverterDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<PriceSource>> GetAllPriceSourcesAsync()
        {
            return
                await _dbContext.PriceSources.AsNoTracking()
                .Include(priceSource => priceSource.availableCurrencyPairs)
                    .ThenInclude(currencyPair => currencyPair.fromCurrency)
                .Include(priceSource => priceSource.availableCurrencyPairs)
                    .ThenInclude(currencyPair => currencyPair.toCurrency)
                .Include(priceSource => priceSource.priceSourceReponses)
                    .ThenInclude(priceSourceResponse => priceSourceResponse.requestedProperty)
                .ToListAsync();
        }
        
        public async Task<PriceSource> GetPriceSourceByIdAsync(int priceSourceId)
        {
            return
                await _dbContext.PriceSources.AsNoTracking()
                .Where(ps => ps.id == priceSourceId)
                .Include(priceSource => priceSource.availableCurrencyPairs)
                    .ThenInclude(currencyPair => currencyPair.fromCurrency)
                .Include(priceSource => priceSource.availableCurrencyPairs)
                    .ThenInclude(currencyPair => currencyPair.toCurrency)
                .Include(priceSource => priceSource.priceSourceReponses)
                    .ThenInclude(priceSourceResponse => priceSourceResponse.requestedProperty)
                .FirstOrDefaultAsync();
        }
    }
}
