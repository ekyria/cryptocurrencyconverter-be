﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Domain.Core.Installers
{
    public interface IInstaller
    {
        void InstallServices<T>(IServiceCollection services, IConfiguration configuration)
            where T : DbContext;
    }
}