﻿using Domain.Core.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Domain.Core.Installers
{
    public class DbInstaller : IInstaller
    {
        public void InstallServices<T>(IServiceCollection services, IConfiguration configuration)
            where T : DbContext
        {
            DbConnectionSettings dbConnectionSettings = new DbConnectionSettings();
            configuration.Bind(nameof(dbConnectionSettings), dbConnectionSettings);
            string host = configuration["DBHOST"] ?? dbConnectionSettings.Host;
            string port = configuration["DBPORT"] ?? dbConnectionSettings.Port;
            string database = configuration["DBNAME"] ?? dbConnectionSettings.Database;
            string userid = configuration["DBUSERID"] ?? dbConnectionSettings.User;
            string password = configuration["DBPASSWORD"] ?? dbConnectionSettings.Password;

            string connectionString = $"server={host}; userid={userid}; pwd={password};" +
                                                $"port={port}; database={database};Convert Zero Datetime=True";
            Console.WriteLine(connectionString);
            services.AddDbContext<T>(options =>
            {
                options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString),
                                                mySqlOptionsAction: MySQLOptions =>
                                                {
                                                    MySQLOptions.EnableRetryOnFailure();
                                                }).EnableSensitiveDataLogging();
            });
        }
    }
}