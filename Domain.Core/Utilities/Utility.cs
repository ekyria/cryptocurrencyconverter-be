﻿using Newtonsoft.Json.Linq;
using System;

namespace Domain.Core.Utilities
{
    public static class Utility
    {
        public static string GetValueFromObject(string responseContent, string responseProperty)
        {
            JObject jObject = JObject.Parse(responseContent);
            return (jObject.SelectToken(responseProperty)).ToString();
        }

        public static DateTime DateTimeConverter(string inString)
        {
            DateTime dateValue;

            if (DateTime.TryParse(inString, out dateValue))
                Console.WriteLine("Converted '{0}' to {1}.", inString, dateValue);

            else if (UnixTimeStampToDateTime(inString, out dateValue))
                Console.WriteLine("Converted '{0}' to {1}.", inString, dateValue);
            else
            {
                Console.WriteLine("Unable to convert '{0}' to a date.", inString);
            }

            return dateValue;
        }
        private static bool UnixTimeStampToDateTime(string inString, out DateTime dateValue)
        {
            dateValue = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);

            try
            {
                double unixTimeStamp = Convert.ToDouble(inString);
                dateValue = dateValue.AddSeconds(unixTimeStamp).ToLocalTime();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
