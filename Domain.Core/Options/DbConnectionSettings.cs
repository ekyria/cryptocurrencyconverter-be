﻿using System;

namespace Domain.Core.Options
{
    public class DbConnectionSettings
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string BuildConnectionString()
        {
            string host = Environment.GetEnvironmentVariable("DBHOST") ?? Host;
            string port = Environment.GetEnvironmentVariable("DBPORT") ?? Port;
            string database = Environment.GetEnvironmentVariable("DBNAME") ?? Database;
            string userid = Environment.GetEnvironmentVariable("DBUSERID") ?? User;
            string password = Environment.GetEnvironmentVariable("DBPASSWORD") ?? Password;

            string connectionString = $"server={host}; userid={userid}; pwd={password};" +
                                                $"port={port}; database={database};Convert Zero Datetime=True";
            return connectionString;
        }
    }
}
