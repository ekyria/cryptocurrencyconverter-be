using CryptoCurrencyConverter.Service.Application.Interfaces;
using CryptoCurrencyConverter.Service.Application.Services;
using CryptoCurrencyConverter.Service.Data.Db;
using CryptoCurrencyConverter.Service.Data.Repositories;
using CryptoCurrencyConverter.Service.Domain.Handlers;
using CryptoCurrencyConverter.Service.Domain.Interfaces;
using Domain.Core.Installers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Linq;

namespace CryptoCurrencyConverter.Service.Api
{
    public class Startup
    {

        private readonly ILogger _logger;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _logger = LogInstaller.ConfigureLogging();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("CorsPolicy", builder => {
                builder.WithOrigins("http://localhost:3000")
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CryptoCurrencyConverter.Service.Api", Version = "v1" });
            });

            var installers = typeof(IInstaller).Assembly.ExportedTypes.Where(x =>
                typeof(IInstaller).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).Select(Activator.CreateInstance).Cast<IInstaller>().ToList();
            installers.ForEach(installer => installer.InstallServices<CryptoConverterDbContext>(services, Configuration));

            services.AddScoped<CryptoConverterDbContext>();
            services.AddHttpClient();

            services.AddScoped<ICryptoToFiatService, CryptoToFiatService>();
            services.AddScoped<IHttpRequestHandler, HttpRequestHandler>();
            services.AddScoped<IPriceSourceRepository, PriceSourceRepository>();
            services.AddScoped<IPriceSourceService, PriceSourceService>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CryptoCurrencyConverter.Service.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
