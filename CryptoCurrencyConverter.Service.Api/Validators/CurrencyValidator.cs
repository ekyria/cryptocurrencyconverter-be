﻿using CryptoCurrencyConverter.Service.Api.Contracts.V1.Requests;
using FluentValidation;

namespace CryptoCurrencyConverter.Service.Api.Validators
{
    public class CurrencyValidator : AbstractValidator<CurrencyRequest>
    {
        public CurrencyValidator()
        {
            RuleFor(x => x.CurrencyData)
                .Must(x => x.Count > 0)
                .WithMessage("At least a currency is required");

            RuleForEach(x => x.CurrencyData).ChildRules(CurrencyData =>
            {
                CurrencyData.RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty()
                .WithMessage("Could not be null or empty");

                CurrencyData.RuleFor(x => x.Symbol)
                .NotNull()
                .NotEmpty()
                .WithMessage("Could not be null or empty");
            
            });

        }
    }
}