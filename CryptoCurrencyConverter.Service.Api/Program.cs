using CryptoCurrencyConverter.Service.Data.Db;
using CryptoCurrencyConverter.Service.Domain.Setup;
using Domain.Core.Installers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace CryptoCurrencyConverter.Service.Api
{
    public class Program
    {
        private readonly ILogger _logger;

        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            
            var logger = LogInstaller.ConfigureLogging();

            using (var serviceScope = host.Services.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<CryptoConverterDbContext>();
                
                if (context.Database.GetPendingMigrations().Any())
                {
                    logger.Information("There are pending migrations");
                    context.Database.Migrate();
                }

                if (!context.Currencies.Any())
                {
                    foreach (var currency in CryptoConverterDb.Currencies)
                    {
                        context.Currencies.Add(currency);
                    }
                    context.SaveChanges();
                    logger.Information("Added data to the Currency table");

                }

                if (!context.RequestedProperties.Any())
                {
                    foreach (var requestedProperty in CryptoConverterDb.RequestedProperties)
                    {
                        context.RequestedProperties.Add(requestedProperty);
                    }
                    context.SaveChanges();
                    logger.Information("Added data to the RequestedProperties table");

                }

                if (!context.PriceSources.Any())
                {
                    foreach (var priceSource in CryptoConverterDb.PriceSources)
                    {
                        context.PriceSources.Add(priceSource);
                    }
                    context.SaveChanges();
                    logger.Information("Added data to the PriceSources table");

                }
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
