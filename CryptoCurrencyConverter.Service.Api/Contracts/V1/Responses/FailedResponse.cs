﻿using System.Collections.Generic;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.Responses
{
    public class FailedResponse
    {
        public IEnumerable<string> errors { get; set; }
        public bool success { get; internal set; }
    }
}