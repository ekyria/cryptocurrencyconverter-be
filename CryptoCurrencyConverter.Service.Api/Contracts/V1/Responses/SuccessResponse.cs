﻿using System.Collections.Generic;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.Responses
{
    public class SuccessResponse<T> where T : class
    {
        public bool success { get; set; }
        public List<T> items { get; set; }
    }
}