﻿using CryptoCurrencyConverter.Service.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs
{
    public class PriceSourceDTO
    {
        public int priceSourceId { get; set; }
        public string priceSourceName { get; set; }
        public List<CurrencyPair> availableCurrencyPair { get; set; }
        public PriceSourceDTO(PriceSource priceSource)
        {
            priceSourceId = priceSource.id;
            priceSourceName = priceSource.name;
            availableCurrencyPair = priceSource.availableCurrencyPairs.Select(a => new CurrencyPair(a.fromCurrency.symbol, a.toCurrency.symbol, priceSource.parameterFormat)).ToList();
        }
    }

    public class CurrencyPair
    {
        private readonly string _fromCurrency;
        private readonly string _toCurrency;
        private readonly string _parameterFormat;

        public CurrencyPair(string fromCurrency, string toCurrency, string parameterFormat)
        {
            _fromCurrency = fromCurrency;
            _toCurrency = toCurrency;
            _parameterFormat = parameterFormat;
        }
        public string currencyPair { get => $"{_fromCurrency}-{_toCurrency}"; }
        public string parameterRequest { get => _parameterFormat.Replace("{fromCurrency}", _fromCurrency).Replace("{toCurrency}", _toCurrency); }
    }
}
