﻿using System.Collections.Generic;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs
{
    public class ResponseDTO<T> where T : class
    {
        public int CurrentPage { get; init; }

        public int TotalItems { get; init; }

        public int TotalPages { get; init; }

        public List<T> Items { get; init; }
    }
}
