﻿using CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator;
using System;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs
{
    public class TickerDTO
    {
        public int priceSourceId { get; set; }
        public string priceSourceName { get; set; }
        public string currencyPair { get; set; }
        public double price { get; set; }
        public DateTime dateTime { get; set; }
        public TickerDTO(IPriceSource priceSource)
        {
            priceSourceId = priceSource.SourceId;
            priceSourceName = priceSource.SourceName;
            price = priceSource.Price;
            dateTime = priceSource.DateTime;
            currencyPair = priceSource.CurrencyPairCode;
        }
    }
}
