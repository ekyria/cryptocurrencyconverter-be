﻿using CryptoCurrencyConverter.Service.Domain.Models;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs
{
    public class CurrencyDTO
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string symbol { get; private set; }
        public CurrencyDTO(Currency currency)
        {
            id = currency.id;
            name = currency.name;
            symbol = currency.symbol;
        }
    }
}
