﻿namespace CryptoCurrencyConverter.Service.Api.Contracts.V1
{
    public static class AppRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" + Version;

        public static class CryptoToFiat
        {
            public const string GetCryptoToFiat = Base + "/cryptoToFiat";
        }

        public static class PriceSource
        {
            public const string GetPriceSources = Base + "/priceSource";
            public const string PostPriceSource = Base + "/priceSource";
            public const string GetPriceSource = Base + "/priceSource/{priceSourceId}";
        }

        public static class Currency
        {
            public const string GetCurrencies = Base + "/currency";
            public const string PostCurrency = Base + "/currency";
            public const string DeleteCurrency = Base + "/currency/currencyId";
        }
    }
}
