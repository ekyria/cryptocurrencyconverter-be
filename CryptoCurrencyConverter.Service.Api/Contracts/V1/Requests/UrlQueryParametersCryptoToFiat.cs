﻿namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.Requests
{
    public class UrlQueryParametersCryptoToFiat
    {
        public int priceSourceId { get; set; }
        public string currencyPairSymbol { get; set; }
    }
}
