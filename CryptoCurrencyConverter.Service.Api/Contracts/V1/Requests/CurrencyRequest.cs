﻿using System.Collections.Generic;

namespace CryptoCurrencyConverter.Service.Api.Contracts.V1.Requests
{

    public class CurrencyRequest
    {
        public List<currency> CurrencyData { get; set; }
        
    }
    public class currency
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
    }
}
