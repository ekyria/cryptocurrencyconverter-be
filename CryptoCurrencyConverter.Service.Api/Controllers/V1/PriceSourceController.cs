﻿using CryptoCurrencyConverter.Service.Api.Contracts.V1;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.Requests;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.Responses;
using CryptoCurrencyConverter.Service.Api.Validators;
using CryptoCurrencyConverter.Service.Application.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Api.Controllers.V1
{
    public class PriceSourceController : Controller
    {
        private readonly IPriceSourceService _priceSourceService;
        private readonly ILogger<PriceSourceController> _logger;

        public PriceSourceController(IPriceSourceService priceSourceService, ILogger<PriceSourceController> logger)
        {
            _priceSourceService = priceSourceService;
            _logger = logger;
        }

        [HttpGet(AppRoutes.PriceSource.GetPriceSources)]
        public async Task<ActionResult<SuccessResponse<PriceSourceDTO>>> GetPriceSources()
        {
            _logger.LogInformation("GetPriceSources request");
            try
            {
                var priceSources = await _priceSourceService.GetPriceSources();

                if (!priceSources.Any())
                {
                    _logger.LogInformation("PriceSources not found");

                    return Ok(new SuccessResponse<PriceSourceDTO>
                    {
                        success = true,
                        items = new List<PriceSourceDTO>()
                    });
                }

                return Ok(new SuccessResponse<PriceSourceDTO>
                {
                    success = true,
                    items = priceSources.Select(x => new PriceSourceDTO(x)).ToList()
                });
            }
            catch (Exception e)
            {
                _logger.LogError($"Error occured trying to get Price Source information{Environment.NewLine}Error message: {e.Message}{Environment.NewLine}More Information: {e.StackTrace}");

                return BadRequest(new FailedResponse
                {
                    success = false,
                    errors = new[]
                        {
                            "Error retrieving data from the database.",
                            e.Message,
                            e.StackTrace
                        }
                });
            }
        }
    }
}
