﻿using CryptoCurrencyConverter.Service.Api.Contracts.V1;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.Requests;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.Responses;
using CryptoCurrencyConverter.Service.Api.Validators;
using CryptoCurrencyConverter.Service.Domain.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Api.Controllers.V1
{
    public class CurrencyController : Controller
    {
        private readonly IRepository<Currency> _currencyRepository;
        private readonly ILogger<Currency> _logger;

        public CurrencyController(IRepository<Currency> currencyRepository, ILogger<Currency> logger)
        {
            _currencyRepository = currencyRepository;
            _logger = logger;
        }

        [HttpGet(AppRoutes.Currency.GetCurrencies)]
        public async Task<ActionResult<SuccessResponse<Currency>>> GetCurrencies()
        {
            try
            {
                var currencies = await _currencyRepository.GetAll().ToListAsync();

                return Ok(new SuccessResponse<CurrencyDTO>()
                {
                    success = true,
                    items = currencies.Select(x => new CurrencyDTO(x)).ToList()
                });
            }
            catch(Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new FailedResponse
                    {
                        errors = new[]
                        {
                            "Error retrieving data from the database.",
                            e.Message,
                            e.StackTrace
                        }
                    });
            }
        }

        [HttpPost(AppRoutes.Currency.PostCurrency)]
        public async Task<ActionResult<SuccessResponse<CurrencyDTO>>> PostCurrency([FromBody] CurrencyRequest currencyRequest)
        {
            CurrencyValidator validationRules = new CurrencyValidator();
            var validationResults = validationRules.Validate(currencyRequest);

            if (!validationResults.IsValid)
            {
                return BadRequest(new FailedResponse
                {
                    success = false,
                    errors = validationResults.Errors.Select(x => $"Property Name: {x.PropertyName} Error Message: {x.ErrorMessage}")
                });
            }

            if (!ModelState.IsValid)
            {
                _logger.LogError("Model State is not valid - Post Request error {ErrorMessage}", ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage)));
                return BadRequest(new FailedResponse
                {
                    success = false,
                    errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            try
            {
                IEnumerable<Currency> currencies = currencyRequest.CurrencyData.Select(x => new Currency { name = x.Name, symbol = x.Symbol });
                var addedCurrencies = await _currencyRepository.AddRangeAsync(currencies);
               
                return Ok(new SuccessResponse<CurrencyDTO>()
                {
                    success = true,
                    items = addedCurrencies.Select(x => new CurrencyDTO(x)).ToList()
                }); 
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new FailedResponse
                    {
                        errors = new[]
                        {
                            e.Message,
                            e.InnerException.Message,
                            e.StackTrace
                        }
                    });
            }
        }

        [HttpDelete(AppRoutes.Currency.DeleteCurrency)]
        public async Task<ActionResult<SuccessResponse<Currency>>> DeleteCurrency(int id)
        {
            try
            {
                var currencies = await _currencyRepository.GetAll().Where(x => x.id == id).ToListAsync();

                if (!currencies.Any())
                {
                    return NotFound();
                }

                await _currencyRepository.DeleteAsync(currencies.First());
                return NoContent();

            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    new FailedResponse
                    {
                        errors = new[]
                        {
                            e.Message,
                            e.InnerException.Message,
                            e.StackTrace
                        }
                    });
            }
        }
    }
}
