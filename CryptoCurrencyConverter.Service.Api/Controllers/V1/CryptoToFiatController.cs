﻿using CryptoCurrencyConverter.Service.Api.Contracts.V1;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.DTOs;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.Requests;
using CryptoCurrencyConverter.Service.Api.Contracts.V1.Responses;
using CryptoCurrencyConverter.Service.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Api.Controllers.V1
{
    public class CryptoToFiatController : Controller
    {
        private readonly ICryptoToFiatService _cryptoToFiatService;
        private readonly ILogger<CryptoToFiatController> _logger;
        public CryptoToFiatController(ICryptoToFiatService cryptoToFiatService, ILogger<CryptoToFiatController> logger)
        {
            _cryptoToFiatService = cryptoToFiatService;
            _logger = logger;
        }

        [HttpGet(AppRoutes.CryptoToFiat.GetCryptoToFiat)]
        public async Task<ActionResult<SuccessResponse<TickerDTO>>> CryptoToFiat([FromQuery] UrlQueryParametersCryptoToFiat query)
        {
            try
            {
                var ticker = await _cryptoToFiatService.GetTickerAsync(query.priceSourceId, query.currencyPairSymbol);
                
                if (ticker is null)
                {
                    return BadRequest(new FailedResponse
                    {
                        success = false,
                        errors = new[]
                        {
                            "Ticker result is null"
                        }
                    });
                }

                return Ok(new SuccessResponse<TickerDTO>
                {
                    success = true,
                    items = new List<TickerDTO>() { new TickerDTO(ticker) }
                });

            }
            catch (System.Exception e)
            {
                return BadRequest(new FailedResponse
                {
                    success = false,
                    errors = new[]
                        {
                            "Error retrieving data from the database.",
                            e.Message,
                            e.StackTrace
                        }
                });
            }
        }
    }
}
