﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoCurrencyConverter.Service.Domain.Models
{
    public class RequestedProperty
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string propertyName { get; set; }

        public virtual ICollection<PriceSourceResponse> priceSourceResponses { get; set; }
    }
}
