﻿using System;

namespace CryptoCurrencyConverter.Service.Domain.Models
{
    public class BaseEntity
    {
        public DateTime createdDate { get; set; }
        public DateTime updatedDate { get; set; }
    }
}