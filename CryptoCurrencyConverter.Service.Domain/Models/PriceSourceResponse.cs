﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoCurrencyConverter.Service.Domain.Models
{
    public class PriceSourceResponse
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int priceSourceId { get; set; }
        public virtual PriceSource priceSource{ get; set; }
        
        public int requestedPropertyId { get; set; }
        public virtual RequestedProperty requestedProperty { get; set; }

        public string responsePropertyName { get; set; }
    }
}
