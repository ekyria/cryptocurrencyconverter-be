﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoCurrencyConverter.Service.Domain.Models
{
    public class Currency
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }

        public virtual ICollection<AvailableCurrencyPair> fromAvailableCurrencyPairs { get; set; }
        public virtual ICollection<AvailableCurrencyPair> toAvailableCurrencyPairs { get; set; }
    }
}
