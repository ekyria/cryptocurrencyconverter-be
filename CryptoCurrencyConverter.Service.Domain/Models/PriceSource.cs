﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoCurrencyConverter.Service.Domain.Models
{
    public class PriceSource : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string parameterFormat { get; set; }

        public virtual ICollection<AvailableCurrencyPair> availableCurrencyPairs { get; set; }
        public virtual ICollection<PriceSourceResponse>  priceSourceReponses{ get; set; }
    }
}
