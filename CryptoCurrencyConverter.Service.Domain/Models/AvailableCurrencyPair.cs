﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoCurrencyConverter.Service.Domain.Models
{
    public class AvailableCurrencyPair
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int priceSourceId { get; set; }
        public virtual PriceSource priceSource { get; set; }

        public int fromCurrencyId { get; set; }
        public int toCurrencyId { get; set; }

        public virtual Currency fromCurrency { get; set; }
        public virtual Currency toCurrency { get; set; }
    }
}
