﻿using CryptoCurrencyConverter.Service.Domain.Models;
using System.Collections.Generic;

namespace CryptoCurrencyConverter.Service.Domain.Setup
{
    public static class CryptoConverterDb
    {
        public static IEnumerable<Currency> Currencies =>
            new Currency[]
            {
                new Currency
                {
                    id = 1,
                    name = "Bitcoin",
                    symbol = "BTC"
                },
                new Currency
                {
                    id = 2,
                    name = "Ethereum",
                    symbol = "ETH"
                },
                new Currency
                {
                    id = 3,
                    name = "Stellar",
                    symbol = "XLM"
                },
                new Currency
                {
                    id = 4,
                    name = "Poligon",
                    symbol = "MATIC"
                },
                new Currency
                {
                    id = 5,
                    name = "Dollar",
                    symbol = "USD"
                }
            };

        
        public static IEnumerable<RequestedProperty> RequestedProperties =>
            new RequestedProperty[]
            {
                new RequestedProperty
                {
                    id = 1,
                    propertyName = "Price"
                },
                new RequestedProperty
                {
                    id = 2,
                    propertyName = "Time"
                }
            };

        public static IEnumerable<PriceSource> PriceSources =>
            new PriceSource[]
            {
                new PriceSource
                {
                    name = "Bitstamp",
                    url = "https://www.bitstamp.net/api/v2/ticker/{currency_p}",
                    parameterFormat = "{fromCurrency}{toCurrency}",
                    priceSourceReponses = new PriceSourceResponse[]
                    {
                        new PriceSourceResponse
                        {
                            requestedPropertyId = 1,
                            responsePropertyName = "last"
                        },
                        new PriceSourceResponse
                        {
                            requestedPropertyId = 2,
                            responsePropertyName = "timestamp"
                        }
                    },
                    availableCurrencyPairs = new AvailableCurrencyPair[]
                    {
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 1,
                            toCurrencyId = 5
                        },
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 2,
                            toCurrencyId = 5
                        },
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 3,
                            toCurrencyId = 5
                        },
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 4,
                            toCurrencyId = 5
                        }
                    }
                },
                new PriceSource
                {
                    name = "Coinbase",
                    url = "https://api.exchange.coinbase.com/products/{currency_p}/ticker",
                    parameterFormat = "{fromCurrency}-{toCurrency}",
                    priceSourceReponses = new PriceSourceResponse[]
                    {
                        new PriceSourceResponse
                        {
                            requestedPropertyId = 1,
                            responsePropertyName = "price"
                        },
                        new PriceSourceResponse
                        {
                            requestedPropertyId = 2,
                            responsePropertyName = "time"
                        }
                    },
                    availableCurrencyPairs = new AvailableCurrencyPair[]
                    {
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 1,
                            toCurrencyId = 5
                        },
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 3,
                            toCurrencyId = 5
                        },
                        new AvailableCurrencyPair
                        {
                            fromCurrencyId = 4,
                            toCurrencyId = 5
                        },
                    }
                }
            };
    }
}
