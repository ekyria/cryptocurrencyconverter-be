﻿using CryptoCurrencyConverter.Service.Domain.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Domain.Handlers
{
    public class HttpRequestHandler : IHttpRequestHandler
    {
        private readonly ILogger _logger;

        public IHttpClientFactory httpClientFactory { get; set; }

        public HttpRequestHandler(ILogger<HttpRequestHandler> logger)
        {
            _logger = logger;
        }
        public async Task<HttpResponseMessage> GetRequestAsync(string requestUri)
        {
            try
            {
                var httpRequestMessage = new HttpRequestMessage( HttpMethod.Get, requestUri)
                {
                    Headers = {
                            { HeaderNames.Accept, "application/json" },
                            { HeaderNames.UserAgent, "HttpRequestsSample" }
                        }};

                var httpClient = httpClientFactory.CreateClient();
                var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    _logger.LogError(httpResponseMessage.RequestMessage.RequestUri.AbsoluteUri);
                    _logger.LogError(httpResponseMessage.ReasonPhrase);
                    _logger.LogInformation($"Status Code {httpResponseMessage.StatusCode}");
                }

                return httpResponseMessage;
            }
            catch (Exception e)
            {
                _logger.LogError($"Cannot send request, {e.Message} {e.InnerException}");
                return null;
            }
        }
    }
}
