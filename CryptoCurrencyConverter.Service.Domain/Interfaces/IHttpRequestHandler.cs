﻿using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Domain.Interfaces
{
    public interface IHttpRequestHandler
    {
        Task<HttpResponseMessage> GetRequestAsync(string requestUri);
        IHttpClientFactory httpClientFactory { get; set; }
    }
}
