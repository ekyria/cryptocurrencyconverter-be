﻿using CryptoCurrencyConverter.Service.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Domain.Interfaces
{
    public interface IPriceSourceRepository : IRepository<PriceSource>
    {
        Task<IEnumerable<PriceSource>> GetAllPriceSourcesAsync();

        Task<PriceSource> GetPriceSourceByIdAsync(int priceSourceId);
    }
}
