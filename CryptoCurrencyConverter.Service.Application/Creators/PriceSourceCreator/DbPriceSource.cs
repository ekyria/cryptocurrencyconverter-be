﻿using CryptoCurrencyConverter.Service.Domain.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Models;
using Domain.Core.Installers;
using Domain.Core.Utilities;
using Serilog;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator
{
    public class DbPriceSource : IPriceSource
    {
        private readonly IHttpRequestHandler _httpRequestHandler;
        private readonly Domain.Models.PriceSource _priceSource;
        private readonly string _currencyPairCode;
        private readonly ILogger _logger;

        public DbPriceSource(IHttpClientFactory httpClientFactory, IHttpRequestHandler httpRequestHandler, Domain.Models.PriceSource priceSource, string currencyPairCode)
        {
            _logger = LogInstaller.ConfigureLogging();

            _httpRequestHandler = httpRequestHandler;
            _httpRequestHandler.httpClientFactory = httpClientFactory;
            _priceSource = priceSource;
            
            _currencyPairCode = currencyPairCode;
        }
        public int SourceId { get => _priceSource.id == 0 ? throw new ArgumentException("Price source id is zero") : _priceSource.id; }
        public string SourceName { get => _priceSource.name ?? throw new ArgumentNullException($"Price source name is null {nameof(DbPriceSource)}"); }
        public string CurrencyPairCode { get => _currencyPairCode ?? throw new ArgumentNullException($"Currency pair code is null {nameof(DbPriceSource)}"); }
        public double Price { get; private set; }
        public DateTime DateTime { get; private set; }

        private string url
        {
            get
            {
                string oldValue = "{currency_p}";
                if (_priceSource.url.Contains(oldValue))
                {
                    return _priceSource.url.Replace(oldValue, _currencyPairCode);
                }
                else
                {
                    _logger.Warning($"Uri does not contain the word {oldValue}");
                    return _priceSource.url;
                }
            }
        }

        private PriceSourceResponse priceResponseProperty { get => _priceSource.priceSourceReponses.FirstOrDefault(x => x.requestedProperty.propertyName == "Price"); }
        private PriceSourceResponse timeResponseProperty { get => _priceSource.priceSourceReponses.FirstOrDefault(x => x.requestedProperty.propertyName == "Time"); }

        public async Task GetTicker()
        {
            var response = await _httpRequestHandler.GetRequestAsync(url);
            var responseContent = await response.Content.ReadAsStringAsync();

            double price;
            if (double.TryParse(Utility.GetValueFromObject(responseContent, priceResponseProperty.responsePropertyName), out price))
                Price = Math.Round(price,2, MidpointRounding.AwayFromZero);
            

            string time = Utility.GetValueFromObject(responseContent, timeResponseProperty.responsePropertyName);
            DateTime = Utility.DateTimeConverter(time);
        }
    }
}
