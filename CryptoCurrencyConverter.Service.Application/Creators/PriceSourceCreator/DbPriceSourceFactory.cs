﻿using CryptoCurrencyConverter.Service.Domain.Interfaces;
using System.Net.Http;

namespace CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator
{
    public class DbPriceSourceFactory : PriceSourceCreator
    {
        private readonly IHttpClientFactory _httpClient;
        private readonly IHttpRequestHandler _httpRequestHandler;
        private readonly Domain.Models.PriceSource _priceSource;
        private readonly string _currencyPairCode;

        public DbPriceSourceFactory(IHttpClientFactory httpClient, IHttpRequestHandler httpRequestHandler, Domain.Models.PriceSource priceSource, string currencyPairCode)
        {
            _httpClient = httpClient;
            _httpRequestHandler = httpRequestHandler;
            _priceSource = priceSource;
            _currencyPairCode = currencyPairCode;
        }
        public override IPriceSource CreatePriceSource()
        {
            return new DbPriceSource(_httpClient, _httpRequestHandler, _priceSource, _currencyPairCode);
        }
    }
}