﻿using System;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator
{
    public interface IPriceSource
    {
        int SourceId { get; }
        string CurrencyPairCode { get; }
        string SourceName { get; }
        double Price { get; }
        DateTime DateTime { get; }

        Task GetTicker();
    }
}
