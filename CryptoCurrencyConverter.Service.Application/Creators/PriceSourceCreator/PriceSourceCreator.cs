﻿using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator
{
    public abstract class PriceSourceCreator
    {
        public abstract IPriceSource CreatePriceSource();

        public async Task<IPriceSource> GetTicker()
        {
            IPriceSource priceSource = CreatePriceSource();
            await priceSource.GetTicker();

            return priceSource;
        }
    }
}
