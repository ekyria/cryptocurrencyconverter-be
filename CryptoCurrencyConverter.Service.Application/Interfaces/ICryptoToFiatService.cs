﻿using CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Interfaces
{
    public interface ICryptoToFiatService
    {
        Task<IPriceSource> GetTickerAsync(int priceSourceId, string currencyPairSymbol);
    }
}
