﻿using CryptoCurrencyConverter.Service.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Interfaces
{
    public interface IPriceSourceService
    {
        Task<IEnumerable<PriceSource>> GetPriceSources();
    }
}
