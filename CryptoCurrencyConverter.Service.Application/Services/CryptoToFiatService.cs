﻿using CryptoCurrencyConverter.Service.Application.Creators.PriceSourceCreator;
using CryptoCurrencyConverter.Service.Application.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Interfaces;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Services
{
    public class CryptoToFiatService : ICryptoToFiatService
    {
        private readonly ILogger<CryptoToFiatService> _logger;
        private readonly IHttpRequestHandler _httpRequestHandler;
        private readonly IHttpClientFactory _httpClient;
        private readonly IPriceSourceRepository _priceSourceRepository;

        public CryptoToFiatService(ILogger<CryptoToFiatService> logger, IHttpRequestHandler httpRequestHandler, IHttpClientFactory httpClient, IPriceSourceRepository priceSourceRepository)
        {
            _logger = logger;
            _httpRequestHandler = httpRequestHandler;
            _httpClient = httpClient;
            _priceSourceRepository = priceSourceRepository;
        }

        public async Task<IPriceSource> GetTickerAsync(int priceSourceId, string currencyPairSymbol)
        {
            PriceSourceCreator priceSourceCreator;

            var priceSource = await _priceSourceRepository.GetPriceSourceByIdAsync(priceSourceId);
            if (priceSource == null)
            {
                _logger.LogError($"Cannot get price source data of the requested Id {priceSourceId}");
                return null;
            }
            priceSourceCreator = new DbPriceSourceFactory(_httpClient, _httpRequestHandler, priceSource, currencyPairSymbol);

            return await priceSourceCreator.GetTicker();
        }
    }
}
