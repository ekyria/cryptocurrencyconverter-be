﻿using CryptoCurrencyConverter.Service.Application.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Interfaces;
using CryptoCurrencyConverter.Service.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoCurrencyConverter.Service.Application.Services
{
    public class PriceSourceService : IPriceSourceService
    {
        private readonly IPriceSourceRepository _priceSourceRepository;

        public PriceSourceService(IPriceSourceRepository priceSourceRepository)
        {
            _priceSourceRepository = priceSourceRepository;
        }

        public async Task<IEnumerable<PriceSource>> GetPriceSources() =>
            await _priceSourceRepository.GetAllPriceSourcesAsync();
    }
}
